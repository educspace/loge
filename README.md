# Loge

Une petite application pour gérer les mises en loge du Grand Oral, développée pendant les phases d'ennui de mes surveillances de loge de Grand Oral 2024...

Accessible à l'adresse [loge.educ.space](https://loge.educ.space) ou [pages forge](https://loge-educspace-8e5d2296d2b632afc9f9d8832225920d7f2f61343f745a87.forge.apps.education.fr/)

Application installable sur mobile ou ordinateur (PWA).

![Loge](./doc/screen.png)


## TODO

Un jour, peut-être...

- [ ] suppression élève / édition nom
- [ ] modification heure de début / fin (+ tiers temps)
- [ ] configuration durées par défaut (normal + 1/3 temps)
- [ ] bip ou clignotant quand fin de temps de préparation
- [ ] bootstrap local (pour PWA offline) + bootswatch ?
- [ ] export CSV
- [ ] suppression / modification de salle





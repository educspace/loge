import {Component, computed, inject} from '@angular/core';
import {ConfigService} from "../../config.service";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";

@Component({
  selector: 'app-candidate-create',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  template: `
    <form [formGroup]="fg" (submit)="submit()" class="row g-3">
      <div class="col-6">
        <label for="name">Candidat</label>
        <input type="text" class="form-control" id="name" formControlName="name">
      </div>
      <div class="col-3" (submit)="submit()">
        <label for="classe">Salle</label>
        <select class="form-select" id="classe" formControlName="room">
          <option [ngValue]="null">---  </option>
          @for (c of rooms(); track c.id) {
            <option [ngValue]="c.id">{{c.name}}</option>
          }
        </select>
      </div>
      <div class="col-3">
        <button type="submit" class="w-100 h-100 btn btn-outline-primary">Ajouter</button>
      </div>
    </form>
  `,
  styles: ``
})
export class CandidateCreateComponent {
  public service = inject(ConfigService);
  public rooms = computed(() => {
    return this.service.config().rooms
  })
  public fg = new FormGroup({
    name: new FormControl<string>('', {nonNullable: true, validators: [Validators.required]}),
    room: new FormControl<number|null>(null)
  });

  public submit() {
    if (this.fg.valid) {
      this.service.addCandidate(this.fg.value)
    }
  }



}

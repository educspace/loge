import {Component, computed, inject} from '@angular/core';
import {CandidateCreateComponent} from "./candidate-create/candidate-create.component";
import {ConfigService} from "../config.service";
import {CandidateDisplayComponent} from "./candidate-display/candidate-display.component";

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    CandidateCreateComponent,
    CandidateDisplayComponent
  ],
  template: `
    <div class="maxh my-3">
  <table class="table table-sm   table-hover">
    <thead>
        <tr>
          <th>Candidat</th>
          <th>Salle</th>
          <th>Début</th>
          <th>Fin</th>
          <th>État</th>
          <th>Action</th>
        </tr>
    </thead>
    <tbody>
      @for (c of candidates(); track c.id) {
        <tr app-candidate-display [candidate]="c"></tr>
      }
    </tbody>
  </table>
    </div>
  <div class="card">
    <div class="card-header">Ajouter un candidat</div>
    <div class="card-body"><app-candidate-create></app-candidate-create></div>
  </div>
  `,
  styles: `
  .maxh {
    max-height: 80vh;
    overflow-y: auto;
  }
  `
})
export class DashboardComponent {
  public candidates = computed(() => {
    return this.service.config().candidats
  })
  public service = inject(ConfigService);
}

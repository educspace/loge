import {Component, inject, input} from '@angular/core';
import {Candidate, CANDIDATE_STATUS} from "../../classes/candidate";
import {DatePipe, DecimalPipe} from "@angular/common";
import {ConfigService} from "../../config.service";

@Component({
  selector: 'tr[app-candidate-display]',
  standalone: true,
  imports: [
    DatePipe,
    DecimalPipe
  ],
  host: {
    '[class.table-secondary]': 'candidate().status === CANDIDATE_STATUS.SORTI',
    '[class.table-warning]': 'candidate().status === CANDIDATE_STATUS.ATTENTE',
    '[class.table-info]': 'candidate().status === CANDIDATE_STATUS.ATTENDU',
  },
  template: `
      <td>
        <span title="Temps restant : {{candidate().timeLeft(service.clock())!/60|number:'2.0-0'}}min">
            {{candidate().name}}
        </span>
        <div class="progress" style="height: 5px;">
          <div class="progress-bar" role="progressbar" [style.width]="candidate().timeLeftPercent(service.clock())+'%'" [attr.aria-valuenow]="candidate().timeLeftPercent(service.clock())" aria-valuemin="0" aria-valuemax="100"></div>
        </div>

      </td>
      <td>
        @if(service.getRoom(candidate().room); as room) {
          <span class="badge bg-dark">{{room.name}}</span>
        }
      </td>
      <td>
        {{candidate().start|date:'shortTime'}}

      </td>
      <td>{{candidate().end|date:'shortTime'}}</td>
      <td>


        @switch (candidate().status) {
          @case (CANDIDATE_STATUS.ATTENDU) {
            <span class="badge bg-info">Non commencé</span>
          }
          @case (CANDIDATE_STATUS.ATTENTE) {
            <span class="badge bg-warning">En attente</span>
          }
          @case (CANDIDATE_STATUS.PREPARATION) {
            <span class="badge bg-primary">En préparation</span>
          }
          @case (CANDIDATE_STATUS.SORTI) {
            <span class="badge bg-success">Sorti</span>
          }


        }
        <td>

        @if (!candidate().start ) {
          <button (click)="service.startCandidate(candidate().id)" class="btn btn-outline-primary w-100 btn-sm">Démarrer</button>
        } @else {
          @if (candidate().status !== CANDIDATE_STATUS.SORTI) {
            <button class="btn btn-outline-success btn-sm w-100" (click)="service.outCandidate(candidate().id)">Sortie</button>
          }
        }
      </td>


  `,
  styles: `

  `
})
export class CandidateDisplayComponent {
  public service = inject(ConfigService);
  public candidate = input.required<Candidate>();
  protected readonly CANDIDATE_STATUS = CANDIDATE_STATUS;
}

import {Component, inject} from '@angular/core';
import {ConfigService} from "../config.service";
import {DecimalPipe} from "@angular/common";

@Component({
  selector: 'app-clock',
  standalone: true,
  imports: [
    DecimalPipe
  ],
  template: `
    <div class="time">{{clock().getHours()|number:'2.0-0'}}:{{clock().getMinutes()|number:'2.0-0'}}:{{clock().getSeconds()|number:'2.0-0'}}</div>
  `,
  styles: `
  .time {
    font-size: calc(50vw/5);
    text-align: center;
  }`
})
export class ClockComponent {
  public readonly clock = inject(ConfigService).clock;
}

import {Injectable, signal} from '@angular/core';
import {Config} from "./classes/config";
import {CandidatData} from "./classes/candidate";
import {SalleData} from "./classes/room";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private current!: Config;
  public readonly clock = signal<Date>(new Date());
  public readonly config = signal<Config>(new Config());

  constructor() {
    this.update();
    setInterval(() => {
      this.clock.set(new Date());
    }, 1000);
  }

  public update() {
    this.current = new Config();
    this.current.load();
    this.config.set(this.current);

  }

  public addCandidate(value: Partial<CandidatData>) {
    this.current.addCandidate(value);
    this.update();
  }

  public addRoom(value: Partial<SalleData>) {
    this.current.addRoom(value);
    this.update();
  }

  public getRoom(id: number|null) {
    return this.current.getRoom(id);
  }

  public startCandidate(candidate: number) {
    this.current.startCandidate(candidate, this.current.base_duration);
    this.update();
  }

  public outCandidate(candidate: number) {
    this.current.outCandidate(candidate);
    this.update();
  }




  public reset() {
    console.log("Resetting current service");
    this.current = new Config();
    this.current.save();
    this.update();
  }
}

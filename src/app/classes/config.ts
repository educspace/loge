import {Candidate, CandidatData} from "./candidate";
import {Room, SalleData} from "./room";

const LOCAL_STORAGE_KEY = 'config';

interface ConfigData {
  title: string;
  base_duration: number;
  alt_duration: number;
  rooms: SalleData[];
  candidates: CandidatData[];
}

export class Config {
  private _title: string = 'Loge';
  public get title(): string {
    return this._title;
  }
  public base_duration: number = 20
  public alt_duration: number = 27
  public rooms: Room[] = [];
  public candidats: Candidate[] = [];


  public fromJSON(json: ConfigData) {
    this._title = json.title || 'Loge'
    this.base_duration = json.base_duration || 20;
    this.alt_duration = json.alt_duration || 27;
    this.rooms = json.rooms.map((s: any) => new Room().fromJSON(s));
    this.candidats = json.candidates.map((c: any) => new Candidate().fromJSON(c));
    return this;
  }

  public toJSON(): ConfigData {
    return {
      title: this._title,
      base_duration: this.base_duration,
      alt_duration: this.alt_duration,
      rooms: this.rooms.map((s: Room) => s.toJSON()),
      candidates: this.candidats.map((c: Candidate) => c.toJSON())
    }
  }


  public load() {
    // Try to load from local storage; if not found, do nothing.
    console.log("Loading config");
    let config = localStorage.getItem(LOCAL_STORAGE_KEY);
    if (config) {
      try {
        this.fromJSON(JSON.parse(config));
      } catch (e) {
        console.error("Error loading config from local storage", e);
      }
    }
  }


  public save() {
    console.log("Saving config");
    this.candidats.sort((a: Candidate, b: Candidate) => {
      return (b.end?.getTime()||(b.created.getTime() + 24*3600*1000)) - (a.end?.getTime()||(a.created.getTime() + 24*3600*1000)) ;
    });
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(this.toJSON()));
  }



  public addCandidate(value: Partial<CandidatData>) {
    this.candidats.push(new Candidate(value));
    this.save();
  }

  public addRoom(value: Partial<SalleData>) {
    this.rooms.push(new Room(value));
    this.save();
  }

  public getRoom(id: number|null) {
    return this.rooms.find((r: Room) => r.id === id);
  }

  public startCandidate(candidate: number, duration: number) {
    let c = this.candidats.find((c: Candidate) => c.id === candidate);
    if (c) {
      c.startNow(duration);
      this.save();
    }
  }

  public outCandidate(candidate: number) {
    let c = this.candidats.find((c: Candidate) => c.id === candidate);
    if (c) {
      c.out();
      this.save();
    }
  }

}


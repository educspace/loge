export enum CANDIDATE_STATUS {
  ATTENDU,
  PREPARATION,
  ATTENTE,
  SORTI
}

export interface CandidatData {
  id: number;
  name: string;
  room: number | null;
  start?: Date;
  end?: Date;
  created: Date;
  status: CANDIDATE_STATUS;
}

export class Candidate {
  private _id: number;
  public get id(): number {
    return this._id;
  }
  private _name: string;
  public get name(): string {
    return this._name;
  }
  private _start?: Date;
  public get start(): Date | undefined {
    return this._start;
  }
  private _end?: Date;
  public get end(): Date | undefined {
    return this._end;
  }
  private _room: number | null;
  public get room(): number | null {
    return this._room;
  }
  private _status: CANDIDATE_STATUS = CANDIDATE_STATUS.ATTENDU;

  public get status(): CANDIDATE_STATUS {
    return this._status;
  }
  private _created: Date = new Date();
  public get created(): Date {
    return this._created;
  }




  constructor(data: Partial<CandidatData> = {}) {
    this._id = data.id || Date.now() *1000 + Math.floor(Math.random() * 1000);
    this._name = data.name || '';
    this._room = data.room || null;
  }

  public toJSON(): CandidatData {
    return {
      id: this._id,
      name: this._name,
      start: this._start,
      end: this._end,
      room: this._room,
      status: this._status,
      created: this._created,
    }
  }
  public fromJSON(json: CandidatData) {
    this._id = json.id;
    this._name = json.name;
    this._start = json.start ? new Date(json.start) : undefined;
    this._end = json.end ? new Date(json.end) : undefined;
    this._room = json.room;
    this._status = json.status;
    this._created = json.created ? new Date(json.created) : new Date();
    this.checkStatus(new Date());
    return this;
  }

  public startNow(duration: number ) {
    this._start = new Date();
    this._end = new Date(Date.now() + 1000 * 60 * duration);
    this._status = CANDIDATE_STATUS.PREPARATION;
  }


  public timeLeft(date: Date): number|null {
    if (this._start && this._end) {
      // Non commencé : on ne donne pas de temps restant
      if (date.getTime() < this._start.getTime()) {
        return null
      }
      const left =  this._end.getTime() - Date.now();
      if (left < 0) {
        return 0;
      }
      return left/1000;
    }
    return null;
  }

  public timeLeftPercent(date: Date): number|null {
    if (this._end && this._start) {
      const pct = (this._end.getTime() - date.getTime()) / (this._end.getTime() - this._start.getTime()) * 100;
      if (pct < 0) {
        return 0;
      }
      if (pct > 100) {
        return 100;
      }
      return Math.round(pct*100)/100;
    }
    return null;
  }

  public checkStatus(date: Date) {
    if (!this._start || !this._end) {
      this._status = CANDIDATE_STATUS.ATTENDU;
    }
    if (this._status != CANDIDATE_STATUS.SORTI && this._start && this._end) {
      if (date.getTime() < this._start.getTime()) {
        this._status = CANDIDATE_STATUS.ATTENDU;
      }
      if (date.getTime() > this._start.getTime() && date.getTime() < this._end.getTime()) {
        this._status = CANDIDATE_STATUS.PREPARATION;
      }
      if (date.getTime() > this._end.getTime()) {
        this._status = CANDIDATE_STATUS.ATTENTE;
      }
    }
  }

  public out() {
    this._status = CANDIDATE_STATUS.SORTI;
  }
}


export interface SalleData {
  id: number;
  name: string;
}
export
class Room {
  private _id: number;
  public get id(): number {
    return this._id;
  }
  private _name: string;
  public get name(): string {
    return this._name;
  }

  constructor(data: Partial<SalleData> = {}) {
    this._id = data.id || Date.now() *1000 + Math.floor(Math.random() * 1000);
    this._name = data.name || ''
  }

  public toJSON(): SalleData {
    return {
      id: this.id,
      name: this._name
    }
  }

  public fromJSON(json: SalleData) {
    this._name = json.name;
    this._id = json.id;
    return this;
  }
}

import {ChangeDetectionStrategy, Component} from '@angular/core';
import {HeaderComponent} from "./header/header.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ClockComponent} from "./clock/clock.component";
import {FooterComponent} from "./footer/footer.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [HeaderComponent, DashboardComponent, ClockComponent, FooterComponent],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <app-header></app-header>
    <div class="container-fluid main">
      <div class="row">
        <div class="col-xl-6 d-flex flex-column justify-content-center">
            <app-clock></app-clock>
        </div>
        <div class="col-xl-6">
            <app-dashboard></app-dashboard>
        </div>
      </div>
    </div>
    <app-footer></app-footer>

  `,
  styles: `
  .main {
    padding-bottom: 70px;
  }`,
})
export class AppComponent {
  title = 'loge';

  constructor() {

  }
}

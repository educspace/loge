import {Component, inject, signal} from '@angular/core';
import {ConfigService} from "../config.service";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  template: `
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-6 d-flex flex-row align-items-center">
            @for (room of service.config().rooms; track room.id) {
              <button class="btn btn-link btn-sm text-info">{{room.name}}</button>
            }
            <button class="btn btn-link btn-sm text-warning" (click)="addRoom.set(true)">+ salle</button>
            @if (addRoom()) {
              <div>
              <form [formGroup]="roomForm" (submit)="createRoom()" class="d-flex flex-row">
                <input type="text" class="form-control form-control-sm" formControlName="name">
                <button class="btn btn-outline-primary">Ajouter</button>
              </form>
              </div>
            }
          </div>
          <div class="col-6 text-center">
            <button class="btn btn-link btn-sm text-warning" (dblclick)="service.reset()" title="Double click pour reset total">reset</button>
          </div>
        </div>
      </div>
    </footer>
  `,
  styles: `
  .footer {
    position: fixed;
    bottom: 0;
    width: 100%;
    height: 60px;
    line-height: 60px;
    background-color: #f5f5f5;
  }
  `,

})
export class FooterComponent {
  public service = inject(ConfigService);
  public roomForm = new FormGroup({
    name: new FormControl<string>('', {nonNullable: true, validators: [Validators.required]})
  });

  public addRoom = signal(true)

  public createRoom() {
    if (this.roomForm.valid) {
      this.service.addRoom(this.roomForm.value)
      this.addRoom.set(false)
    }
  }
}

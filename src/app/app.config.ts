import {
  ApplicationConfig,
  LOCALE_ID,
  provideExperimentalZonelessChangeDetection, isDevMode,
} from '@angular/core';
import { provideServiceWorker } from '@angular/service-worker';


export const appConfig: ApplicationConfig = {
  providers: [provideExperimentalZonelessChangeDetection(), { provide: LOCALE_ID, useValue: 'fr'}, provideServiceWorker('ngsw-worker.js', {
            enabled: !isDevMode(),
            registrationStrategy: 'registerWhenStable:30000'
          })]
};
